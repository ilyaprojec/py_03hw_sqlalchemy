from sqlalchemy import create_engine
from models import Base
from sqlalchemy.orm import sessionmaker

engine = create_engine("sqlite:///db.sqlite3", echo=True)

session = sessionmaker(bind=engine)()

if __name__ == '__main__':
    Base.metadata.create_all(engine)
