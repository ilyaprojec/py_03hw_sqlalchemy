from flask import Flask, render_template, request
from forms import BookForm, AuthorForm, GenreForm
from models import Author, Book, Genre
from db import session


app = Flask(__name__)


def create_form_handler(form_class, model_class, title):
    form = form_class()
    success = False
    if request.method == 'POST':
        form = form_class(request.form)
        if form.validate():
            obj = model_class()
            form.populate_obj(obj)
            session.add(obj)
            session.commit()
            success = True

    return render_template(
        'create_book.html', **{
            'form': form,
            'title': title,
            'success': success
        }
    )


@app.route('/')
def home():
    books = session.query(Book).all()

    if request.method == 'GET' and request.values.get('year'):
        books = session.query(Book).filter_by(year=request.values.get('year')).all()

    if request.method == 'GET' and request.values.get('genre'):
        books = session.query(Book).filter(Book.genre == request.values.get('genre')).all()

    return render_template(
        'index.html', **{'books': books}
    )


@app.route("/update/book/<int:book_id>", methods=['GET', 'POST'])
def update_book(book_id):
    book = session.query(Book).filter_by(id=book_id).first()
    form = BookForm(obj=book)

    if request.method == 'POST':
        session.query(Book).filter(Book.id == book_id).update({
            'name': request.form.get('name'),
            'genre': request.form.get('genre_obj'),
            'author': request.form.get('author_obj'),
            'year': request.form.get('year'),
            'content': request.form.get('content'),
        }, synchronize_session="fetch")

        form = BookForm(request.form)
        obj = Book()
        form.populate_obj(obj)

    return render_template(
        'create_book.html', **{
            'form': form,
            'title': 'Update Book',
        }
    )


@app.route('/create/book/', methods=['GET', 'POST'])
def create_book():
    return create_form_handler(BookForm, Book, "Add Book")


@app.route('/create/author/', methods=['GET', 'POST'])
def create_author():
    return create_form_handler(AuthorForm, Author, "Add Author")


@app.route('/create/genre/', methods=['GET', 'POST'])
def create_genre():
    return create_form_handler(GenreForm, Genre, "Add Genre")


if __name__ == '__main__':
    app.run(debug=True)
