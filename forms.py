from wtforms.ext.sqlalchemy.orm import model_form
from models import Book, Author, Genre
from db import session


BookForm = model_form(Book, db_session=session)
AuthorForm = model_form(Author)
GenreForm = model_form(Genre)
